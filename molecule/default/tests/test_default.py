import os
import testinfra.utils.ansible_runner
import pwd

pkg_name='nginx'
svc_name='nginx'
docker_user='www-data' or 'nginx'
docker_group='www-data' or 'nginx'
nginx_folder='/etc/nginx'

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")

def test_nginx_directory_exists(host):
    nginx_dir_exists = host.file("/etc/nginx").exists
    assert nginx_dir_exists

def docker_is_installed(host):
    docker = host.package(pkg_name)
    assert docker.is_installed

def docker_running_and_enabled(host):
    docker = host.service(svc_name)
    assert docker.is_running
    assert docker.is_enabled
    
def test_docker_user_exists(host):
    user_exists = False
    target_users = ['www-data', 'nginx']
    for user_info in pwd.getpwall():
        if user_info.pw_name in target_users:
            user_exists = True
            break
    assert user_exists

def test_docker_group_exists(host):
    group_exists = False
    target_groups = ['www-data', 'nginx']
    for group_info in grp.getgrall():
        if group_info.gr_name in target_groups:
            group_exists = True
            break
    assert group_exists
    
